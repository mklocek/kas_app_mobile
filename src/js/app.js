var API = "http://0.0.0.0:3000/api/v1/";

var API_HEADERS = {
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
  'Access-Control-Allow-Headers': 'Content-Type, X-Requested-With',
};

angular.module('KrakowskiAlarmSmogowy', [
  'ngRoute',
  'ngTouch',
  'mobile-angular-ui',
  'KrakowskiAlarmSmogowy.controllers.Main',
  'KrakowskiAlarmSmogowy.services.playerService'
])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/', { templateUrl: 'home.html' });
}]);