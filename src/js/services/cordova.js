angular.module('KarkowskiAlarmSmogowy.services.Cordova', [])

.factory('deviceReady', function(){
  return function(done) {
    if (typeof window.cordova === 'object') {
      document.addEventListener('deviceready', function () {
        alert("Cordova is ready!");
        done();
      }, false);
    } else {
      done();
    }
  };
});