angular.module('KrakowskiAlarmSmogowy.services.playerService', [])

.config(['$httpProvider', function($httpProvider) {
  $httpProvider.defaults.useXDomain = true;
  $httpProvider.defaults.withCredentials = true;
  delete $httpProvider.defaults.headers.common["X-Requested-With"];
  $httpProvider.defaults.headers.common['Access-Control-Allow-Headers'] = '*';
}])

.factory(

  "playerService",
  
  function( $http, $q ) {
    return ({
      addPlayer:  addPlayer,
      getPlayers: getPlayers
    });

    function addPlayer( player ) {
      var request = $http({
        method: "post",
        url: API + "players/create",
        params: {
          action: "player"
        },
        data: {
          name: player
        },
        headers: API_HEADERS
      });
      return( request.then( handleSuccess, handleError ) );
    }

    function getPlayers() {
      var request = $http({
        method: "get",
        url: API + "players",
        headers: API_HEADERS
      });
      return( request.then( handleSuccess, handleError ) );
    }

    function handleError( response ) {
      if (
          ! angular.isObject( response.data ) ||
          ! response.data.message
        ) {
          return( $q.reject( "An unknown error occurred." ) );
        }
      return( $q.reject( response.data.message ) );
    }

    function handleSuccess( response ) {
      alert(response.data);
      return( response.data );
    };

  }

)
