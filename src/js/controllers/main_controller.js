angular.module('KrakowskiAlarmSmogowy.controllers.Main', [])

.controller('MainController', function($scope, playerService){
  
  $scope.players = [];

  $scope.form = {
    nickname: "",
    age: 0,
    gender: true,
    phone_uuid: ""
  }

  loadRemoteData();

  $scope.addPlayer = function() {
    playerService.addPlayer($scope.form)
      .then(
        loadRemoteData,
        function( errorMessage ) {
          console.warn( errorMessage );
        }
      )
    ;
    $scope.form = {
      nickname: "",
      age: 0,
      gender: true,
      phone_uuid: ""
    }
  };

  function applyRemoteData( newPlayers ) {
    $scope.players = newPlayers;
  }

  function loadRemoteData() {
    playerService.getPlayers()
      .then(
        function( players ) {
          applyRemoteData( players );
        }
      )
    ;
  }

});